
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/appooti/projects/SGR/sgr/Server/conf/routes
// @DATE:Sun Nov 08 23:09:45 IST 2015


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
